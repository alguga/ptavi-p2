import sys


class Calculadora:
    def __init__(self, op1, op2):
        self.op1 = op1
        self.op2 = op2

    def plus(self):
        """ Function to sum the operands. Ops have to be ints """
        return self.op1 + self.op2

    def minus(self):
        """ Function to substract the operands """
        return self.op1 - self.op2


if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])

    except ValueError:
        sys.exit("Error: Non numerical parameters")

    if sys.argv[2] == "suma":
        result = Calculadora(operando1, operando2).plus()

    elif sys.argv[2] == "resta":
        result = Calculadora(operando1, operando2).minus()

    else:
        sys.exit('Operación sólo puede ser suma o resta.')

    print(result)
