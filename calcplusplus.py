import sys
import calcoohija
import csv

if __name__ == "__main__":
    try:
        nombre_fichero = sys.argv[1]

    except IndexError:
        sys.exit("Error: introduce el nombre del fichero")

    with open(nombre_fichero) as csvfile:
        lineas = csv.reader(csvfile)
        for lista in lineas:
            result = int(lista[1])
            if lista[0] == "suma":
                for i in lista[2:]:
                    result = calcoohija.CalculadoraHija(result, int(i)).plus()
                print(result)

            elif lista[0] == "resta":
                for i in lista[2:]:
                    result = calcoohija.CalculadoraHija(result, int(i)).minus()
                print(result)

            elif lista[0] == "divide":
                for i in lista[2:]:
                    result = calcoohija.CalculadoraHija(result, int(i)).div()
                print(result)

            elif lista[0] == "multiplica":
                for i in lista[2:]:
                    result = calcoohija.CalculadoraHija(result, int(i)).mult()
                print(result)
            else:
                sys.exit('Operación sólo puede ser suma , resta, '
                         'multiplca o divide')
