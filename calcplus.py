import sys
import calcoohija

if __name__ == "__main__":
    try:
        nombre_fichero = sys.argv[1]

    except IndexError:
        sys.exit("Error: introduce el nombre del fichero")

    fich = open(nombre_fichero)
    lineas = fich.readlines()

    for linea in lineas:
        lista = linea.split(',')
        result = int(lista[1])
        if lista[0] == "suma":
            for i in lista[2:]:
                result = calcoohija.CalculadoraHija(result, int(i)).plus()
            print(result)

        elif lista[0] == "resta":
            for i in lista[2:]:
                result = calcoohija.CalculadoraHija(result, int(i)).minus()
            print(result)

        elif lista[0] == "divide":
            for i in lista[2:]:
                result = calcoohija.CalculadoraHija(result, int(i)).div()
            print(result)

        elif lista[0] == "multiplica":
            for i in lista[2:]:
                result = calcoohija.CalculadoraHija(result, int(i)).mult()
            print(result)
        else:
            sys.exit('Operación sólo puede ser suma ,'
                     ' resta, multiplca o divide')

    fich.close()
