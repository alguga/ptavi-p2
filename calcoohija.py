import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):

    def mult(self):
        """ Function to sum the operands. Ops have to be ints """
        return self.op1 * self.op2

    def div(self):
        """ Function to substract the operands """
        try:
            resultado = self.op1 / self.op2
            return resultado

        except ZeroDivisionError:
            sys.exit("Division by zero is not allowed")


if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    if sys.argv[2] == "suma":
        result = CalculadoraHija(operando1, operando2).plus()

    elif sys.argv[2] == "resta":
        result = CalculadoraHija(operando1, operando2).minus()

    elif sys.argv[2] == "multiplica":
        result = CalculadoraHija(operando1, operando2).mult()

    elif sys.argv[2] == "divide":
        result = CalculadoraHija(operando1, operando2).div()

    else:
        sys.exit('Operación sólo puede ser suma o resta.')

    print(result)
